package com.gome.club.scores;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan(basePackages = {"com.gome.club.scores.dao"})
@SpringBootApplication
public class ScoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScoresApplication.class, args);
	}

}

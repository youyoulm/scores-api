package com.gome.club.scores.dao;

import com.gome.club.scores.dto.ScoresListReq;
import com.gome.club.scores.entity.Scores;
import com.gome.club.scores.vo.ScoresVo;

import java.util.List;

public interface ScoresMapper {
    /**
     * 获取成绩列表
     * @param req
     * @return
     */
    List<ScoresVo> getScoresList(ScoresListReq req);

    /**
     * 获取成绩信息
     * @param id
     * @return
     */
    Scores getScoresById(Long id);

    /**
     * 添加成绩信息
     * @param record
     * @return
     */
    Integer addScores(Scores record);

    /**
     * 批量添加成绩信息
     * @param record
     * @return
     */
    Integer addBatchScores(List<Scores> record);
    /**
     * 修改成绩信息
     * @param record
     * @return
     */
    Integer updateScores(Scores record);

    /**
     * 删除成绩信息
     * @param id
     * @return
     */
    Integer deleteScores(Long id);

    // int insert(Scores record);

    // int updateByPrimaryKey(Scores record);
}
package com.gome.club.scores.dao;

import com.gome.club.scores.dto.StudentListReq;
import com.gome.club.scores.entity.Student;
import com.gome.club.scores.vo.StudentVo;

import java.util.List;

public interface StudentMapper {

    /**
     * 获取学生列表
     * @param req
     * @return
     */
    List<StudentVo> getStudentList(StudentListReq req);

    /**
     * 获取学生信息
     * @param id
     * @return
     */
    Student getStudentById(Long id);

    /**
     * 添加学生信息
     * @param record
     * @return
     */
    Integer addStudent(Student record);

    /**
     * 批量添加学生信息
     * @param record
     * @return
     */
    Integer addBatchStudent(List<Student> record);
    /**
     * 修改学生信息
     * @param record
     * @return
     */
    Integer updateStudent(Student record);

    /**
     * 删除学生信息
     * @param id
     * @return
     */
    Integer deleteStudent(Long id);

    // int insert(Student record);

    // int updateByPrimaryKey(Student record);
}
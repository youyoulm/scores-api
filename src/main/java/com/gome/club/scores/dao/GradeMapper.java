package com.gome.club.scores.dao;

import com.gome.club.scores.dto.GradeListReq;
import com.gome.club.scores.entity.Grade;
import com.gome.club.scores.vo.GradeVo;

import java.util.List;

public interface GradeMapper {

    /**
     * 获取班级列表
     * @param req
     * @return
     */
    List<GradeVo> getGradeList(GradeListReq req);

    /**
     * 获取班级信息
     * @param id
     * @return
     */
    Grade getGradeById(Integer id);

    /**
     * 添加班级信息
     * @param record
     * @return
     */
    Integer addGrade(Grade record);

    /**
     * 修改班级信息
     * @param record
     * @return
     */
    Integer updateGrade(Grade record);

    /**
     * 删除班级信息
     * @param id
     * @return
     */
    Integer deleteGrade(Integer id);

    // int insert(Grade record);

    // int updateByPrimaryKey(Grade record);
}
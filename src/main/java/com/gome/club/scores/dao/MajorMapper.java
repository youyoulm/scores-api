package com.gome.club.scores.dao;

import com.gome.club.scores.dto.MajorListReq;
import com.gome.club.scores.entity.Major;
import com.gome.club.scores.vo.MajorVo;

import java.util.List;

public interface MajorMapper {
    /**
     * 获取专业列表
     * @param req
     * @return
     */
    List<MajorVo> getMajorList(MajorListReq req);

    /**
     * 获取专业信息
     * @param id
     * @return
     */
    Major getMajorById(Integer id);

    /**
     * 添加专业信息
     * @param record
     * @return
     */
    Integer addMajor(Major record);

    /**
     * 修改专业信息
     * @param record
     * @return
     */
    Integer updateMajor(Major record);

    /**
     * 删除专业信息
     * @param id
     * @return
     */
    Integer deleteMajor(Integer id);

    // int insert(Major record);

    // int updateByPrimaryKey(Major record);
}
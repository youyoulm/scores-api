package com.gome.club.scores.dao;

import com.gome.club.scores.dto.TeacherListReq;
import com.gome.club.scores.entity.Teacher;
import com.gome.club.scores.vo.TeacherVo;

import java.util.List;

public interface TeacherMapper {

    /**
     * 获取教师列表
     * @param req
     * @return
     */
    List<TeacherVo> getTeacherList(TeacherListReq req);

    /**
     * 获取教师信息
     * @param id
     * @return
     */
    Teacher getTeacherById(Long id);

    /**
     * 添加教师信息
     * @param record
     * @return
     */
    Integer addTeacher(Teacher record);

    /**
     * 修改教师信息
     * @param record
     * @return
     */
    Integer updateTeacher(Teacher record);

    /**
     * 删除教师信息
     * @param id
     * @return
     */
    Integer deleteTeacher(Long id);

    // int insert(Teacher record);

    // int updateByPrimaryKey(Teacher record);
}
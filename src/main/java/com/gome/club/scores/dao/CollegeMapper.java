package com.gome.club.scores.dao;

import com.gome.club.scores.dto.CollegeListReq;
import com.gome.club.scores.entity.College;
import com.gome.club.scores.vo.CollegeVo;

import java.util.List;

public interface CollegeMapper {

    /**
     * 获取学院列表
     * @param req
     * @return
     */
    List<CollegeVo> getCollegeList(CollegeListReq req);

    /**
     * 获取学院信息
     * @param id
     * @return
     */
    College getCollegeById(Integer id);

    /**
     * 添加学院信息
     * @param record
     * @return
     */
    Integer addCollege(College record);

    /**
     * 修改学院信息
     * @param record
     * @return
     */
    Integer updateCollege(College record);

    /**
     * 删除学院信息
     * @param id
     * @return
     */
    Integer deleteCollege(Integer id);


    // int insert(College record);

    // int updateByPrimaryKey(College record);
}
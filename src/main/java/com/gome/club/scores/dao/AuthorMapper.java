package com.gome.club.scores.dao;

import com.gome.club.scores.dto.AuthorCheckReq;
import com.gome.club.scores.dto.AuthorListReq;
import com.gome.club.scores.entity.Author;

import java.util.List;

public interface AuthorMapper {

    /**
     * 获取账号列表
     */
    List<Author> getAuthorList(AuthorListReq req);

    /**
     * 获取账号信息
     * @param req
     * @return
     */
    Author getAuthorByName(AuthorCheckReq req);

    /**
     * 删除账号
     * @param userName
     * @return
     */
    int deleteAuthor(String userName);

    /**
     * 添加账号
     * @param record
     * @return
     */
    int addAuthor(Author record);

    /**
     * 修改账号
     * @param record
     * @return
     */
    int updateAuthor(Author record);

    // int insert(Author record);

    // int updateByPrimaryKey(Author record);
}
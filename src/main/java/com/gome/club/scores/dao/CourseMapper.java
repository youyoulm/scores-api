package com.gome.club.scores.dao;

import com.gome.club.scores.dto.CourseListReq;
import com.gome.club.scores.entity.Course;
import com.gome.club.scores.vo.CourseVo;

import java.util.List;

public interface CourseMapper {
    /**
     * 获取课程列表
     * @param req
     * @return
     */
    List<CourseVo> getCourseList(CourseListReq req);

    /**
     * 获取课程信息
     * @param id
     * @return
     */
    Course getCourseById(Integer id);

    /**
     * 添加课程信息
     * @param record
     * @return
     */
    Integer addCourse(Course record);

    /**
     * 修改课程信息
     * @param record
     * @return
     */
    Integer updateCourse(Course record);

    /**
     * 删除课程信息
     * @param id
     * @return
     */
    Integer deleteCourse(Integer id);


    // int insert(Course record);

    // int updateByPrimaryKey(Course record);

}
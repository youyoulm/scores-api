/**
 * FileName: Teacher
 * Author:   liluming
 * Date:     2020/7/27 2:22 下午
 * Description: 教师实体类
 */
package com.gome.club.scores.vo;

import lombok.Data;

import java.util.Date;

@Data
public class TeacherVo {
	private Long id;
	private String teacherNo;
	private String teacherName;
	private Integer sex;
	private String mobile;
	private String email;
	private String collegeName;
	private Date createTime;
	private Date updateTime;
}
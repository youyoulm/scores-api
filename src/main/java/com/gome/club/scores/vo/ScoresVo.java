package com.gome.club.scores.vo;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class ScoresVo {
    private Long id;

    private String studentNo;

    private String studentName;

    private String courseNo;

    private String courseName;

    private String semester;

    private Integer score;

    private String classNo;

    private Date createTime;

    private Date updateTime;
}
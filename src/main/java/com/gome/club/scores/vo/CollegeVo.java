/**
 * FileName: Teacher
 * Author:   liluming
 * Date:     2020/7/27 2:22 下午
 * Description: 教师实体类
 */
package com.gome.club.scores.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CollegeVo {
	private Integer id;

	private String collegeName;

	private String collegeNo;
}
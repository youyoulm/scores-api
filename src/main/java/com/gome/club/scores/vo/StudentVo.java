/**
 * FileName: Teacher
 * Author:   liluming
 * Date:     2020/7/27 2:22 下午
 * Description: 教师实体类
 */
package com.gome.club.scores.vo;

import lombok.Data;

import java.util.Date;

@Data
public class StudentVo {
	private Long id;
	private String studentNo;
	private String studentName;
	private Integer sex;
	private String mobile;
	private String email;
	private String classNo;
	private String className;
	private Date createTime;
	private Date updateTime;
}
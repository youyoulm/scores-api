/**
 * FileName: TeacherAddReq
 * Author:   liluming
 * Date:     2020/7/28 1:46 下午
 * Description: 列表查询对象
 */
package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class TeacherOpReq implements Serializable {

	private static final long serialVersionUID = -7155728210646784053L;

	private Long id;

	private String teacherNo;

	private String teacherName;

	private Integer sex;

	private String mobile;

	private String email;

	private String collegeNo;

	private Date createTime;

	private Date updateTime;
}
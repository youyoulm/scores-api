package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class MajorOpReq {
    private Integer id;

    private String majorName;

    private String majorNo;

    private String collegeNo;

    private Date createTime;

    private Date updateTime;
}
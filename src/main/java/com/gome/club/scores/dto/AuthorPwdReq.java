package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AuthorPwdReq {
    private Long id;

    private String oldPassword;

    private String newPassword;

    private String againPassword;
}
package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class StudentOpReq {
    private Long id;

    private String studentNo;

    private String studentName;

    private Integer sex;

    private String mobile;

    private String email;

    private String classNo;

    private Date createTime;

    private Date updateTime;
}
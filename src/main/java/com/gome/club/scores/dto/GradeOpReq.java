package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class GradeOpReq {
    private Integer id;

    private String gradeName;

    private String gradeNo;

    private String majorNo;

    private String headmaster;

    private Date createTime;

    private Date updateTime;
}
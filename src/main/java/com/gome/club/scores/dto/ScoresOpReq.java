package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class ScoresOpReq {
    private Long id;

    private String studentNo;

    private String courseNo;

    private String classNo;

    private Integer score;

    private Date createTime;

    private Date updateTime;
}
package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class CollegeOpReq {
    private Integer id;

    private String collegeName;

    private String collegeNo;
}
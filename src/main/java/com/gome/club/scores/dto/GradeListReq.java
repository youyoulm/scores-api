/**
 * FileName: TeacherListReq
 * Author:   liluming
 * Date:     2020/7/28 1:46 下午
 * Description: 列表查询对象
 */
package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class GradeListReq {

	private String gradeNo;

	private String majorNo;

	private Integer pageNum;

	private Integer pageSize;
}
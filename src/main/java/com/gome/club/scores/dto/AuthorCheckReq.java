package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AuthorCheckReq {
    private Long id;

    private String userName;

    private String password;

    private String captcha;
}
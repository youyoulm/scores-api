/**
 * FileName: TeacherListReq
 * Author:   liluming
 * Date:     2020/7/28 1:46 下午
 * Description: 列表查询对象
 */
package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TeacherListReq {

	private String teacherNo;

	private String teacherName;

	private String startTime;

	private String endTime;

	private Integer pageNum;

	private Integer pageSize;
}
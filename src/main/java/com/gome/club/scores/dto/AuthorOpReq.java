package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class AuthorOpReq {
    private Long id;

    private String userName;

    private String password;

    private Integer role;
}
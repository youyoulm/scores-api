package com.gome.club.scores.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class CourseOpReq {
    private Integer id;

    private String courseName;

    private String courseNo;

    private String classNo;

    private String semester;

    private String teacherNo;

    private Date createTime;

    private Date updateTime;
}
/**
 * FileName: LoginConfig
 * Author:   liluming
 * Date:     2020/8/14 2:17 下午
 * Description: 登录拦截器配置
 */
package com.gome.club.scores.config;


import com.gome.club.scores.common.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @desc 登录拦截器配置 *
 * @author liluming
 * @create_date 2020/8/14 2:17 下午
 * @version 1.0
 */
@Configuration
public class LoginConfig implements WebMvcConfigurer {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		//注册LoginInterceptor拦截器
		InterceptorRegistration registration = registry.addInterceptor(new LoginInterceptor());

		//对所有路径拦截
		registration.addPathPatterns("/sys/**");

		//添加不拦截路径
		registration.excludePathPatterns(
			"/sys/author/login",     //登录地址
			"/sys/captcha/info",     //验证码
			"/**/*.html",            //html静态资源
			"/**/*.js",              //js静态资源
			"/**/*.css",             //css静态资源
			"/**/*.woff",
			"/**/*.ttf"
		);
	}
}
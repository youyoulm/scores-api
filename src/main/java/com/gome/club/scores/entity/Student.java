package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class Student implements Serializable {

    private static final long serialVersionUID = 6780143507734571321L;
    private Long id;

    private String studentNo;

    private String studentName;

    private Integer sex;

    private String mobile;

    private String email;

    private String classNo;

    private Date createTime;

    private Date updateTime;
}
package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class Author implements Serializable {

    private static final long serialVersionUID = 6152486779825792069L;
    private Long id;

    private String userName;

    private String password;

    private Integer role;
}
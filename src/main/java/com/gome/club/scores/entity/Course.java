package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
@Data
@ToString
public class Course implements Serializable {

    private static final long serialVersionUID = -850643982499024616L;
    private Integer id;

    private String courseName;

    private String courseNo;

    private String classNo;

    private String semester;

    private String teacherNo;

    private Date createTime;

    private Date updateTime;
}
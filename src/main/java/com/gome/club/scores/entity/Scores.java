package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class Scores implements Serializable {

    private static final long serialVersionUID = -401290761796394995L;
    private Long id;

    private String studentNo;

    private String courseNo;

    private String classNo;

    private Integer score;

    private Date createTime;

    private Date updateTime;
}
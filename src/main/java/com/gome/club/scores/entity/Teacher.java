package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class Teacher implements Serializable {

    private static final long serialVersionUID = 4085503949144169226L;
    private Long id;

    private String teacherNo;

    private String teacherName;

    private Integer sex;

    private String mobile;

    private String email;

    private String collegeNo;

    private Date createTime;

    private Date updateTime;
}
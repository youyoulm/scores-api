package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class Major implements Serializable {

	private static final long serialVersionUID = 6033528042612183153L;
	private Integer id;

    private String majorName;

    private String majorNo;

    private String collegeNo;

	private Date createTime;

	private Date updateTime;
}
package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class College implements Serializable {

    private static final long serialVersionUID = -4539714991647520990L;
    private Integer id;

    private String collegeName;

    private String collegeNo;
}
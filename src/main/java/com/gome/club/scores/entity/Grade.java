package com.gome.club.scores.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class Grade implements Serializable {

    private static final long serialVersionUID = 4074707651736488729L;
    private Integer id;

    private String gradeName;

    private String gradeNo;

    private String majorNo;

    private String headmaster;

    private Date createTime;

    private Date updateTime;
}
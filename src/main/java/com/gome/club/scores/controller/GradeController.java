/**
 * FileName: GradeController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 班级管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.GradeListReq;
import com.gome.club.scores.dto.GradeOpReq;
import com.gome.club.scores.service.IGradeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/grade/")
public class GradeController {

	@Autowired
	private IGradeService iGradeService;

	/**
	 * 获取班级列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getGradeList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "gradeNo",required = false) String gradeNo,
			@RequestParam(name = "majorNo",required = false) String majorNo
	){
		GradeListReq gradeListReq = new GradeListReq();
		gradeListReq.setPageNum(pageNum);
		gradeListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(majorNo)){
			gradeListReq.setMajorNo(majorNo);
		}

		if(StringUtils.isNotBlank(gradeNo)){
			gradeListReq.setGradeNo(gradeNo);
		}

		return iGradeService.getGradeList(gradeListReq);
	}

	/**
	 * 查看班级信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getGradeInfo(
			@RequestParam(name = "id",required = false) Integer id
	){
		return iGradeService.getGradeById(id);
	}


	/**
	 * 添加班级信息
	 * @param gradeOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addGrade(
			@RequestBody GradeOpReq gradeOpReq
	){
		return iGradeService.addGrade(gradeOpReq);
	}

	/**
	 * 修改班级信息
	 */
	@PostMapping("update")
	public APIResult updateGrade(
			@RequestBody GradeOpReq gradeOpReq
	){
		return iGradeService.updateGrade(gradeOpReq);
	}

	/**
	 * 删除班级信息
	 */
	@PostMapping("delete")
	public APIResult deleteGrade(
			@RequestBody GradeOpReq gradeOpReq
	){
		if(null == gradeOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iGradeService.deleteGrade(gradeOpReq.getId());
	}
}
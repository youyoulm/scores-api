/**
 * FileName: MajorController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 专业管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.*;
import com.gome.club.scores.dto.*;
import com.gome.club.scores.entity.*;
import com.gome.club.scores.service.ICollegeService;
import com.gome.club.scores.service.IScoresService;
import com.gome.club.scores.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

@RestController
@RequestMapping(value = "/sys/init/")
public class InitDataController {

	@Autowired
	private ICollegeService iCollegeService;

	@Autowired
	private MajorMapper majorMapper;

	@Autowired
	private GradeMapper gradeMapper;

	@Autowired
	private StudentMapper studentMapper;

	@Autowired
	private CourseMapper courseMapper;

	@Autowired
	private ScoresMapper scoresMapper;

	@Autowired
	private IScoresService scoresService;
	/**
	 * 初始化专业列表数据
	 * @return
	 */
	@GetMapping("major")
	public APIResult initMajorData(
	){
		//获取所有学院
		CollegeListReq req = new CollegeListReq();
		req.setPageNum(1);
		req.setPageSize(20);
		PageResult<List<CollegeVo>>  collegeList = iCollegeService.getCollegeList(req);
		List<CollegeVo> collegeVos = collegeList.getData();
		MajorOpReq majorOpReq = new MajorOpReq();
		for (CollegeVo item:collegeVos) {
			//为id大于3的学院生成(学院下的专业)测试数据,每个学院11条
			if(item.getId() > 3){
				for (int i = 1; i < 12; i++) {
					String majorNo = String.format("%02d",i);
					majorOpReq.setMajorNo(item.getCollegeNo()+majorNo);
					majorOpReq.setMajorName(item.getCollegeName()+"-专业"+i);
					majorOpReq.setCollegeNo(item.getCollegeNo());
					// iMajorService.addMajor(majorOpReq);
					// System.out.println(major.toString());
				}
			}

		}
		return APIResult.newSuccessResult("成功");
	}

	/**
	 * 初始化班级数据同步操作
	 * 遍历所有专业，每个专业2个班 20163月份
	 * @return
	 */
	@GetMapping("gradeSingle")
	public APIResult initClassDataBySingle(
	){
		long startTime = System.currentTimeMillis();

		//获取所有专业
		MajorListReq req = new MajorListReq();
		List<MajorVo> majorVos = majorMapper.getMajorList(req);

		Integer classNo = 2015004;
		Grade grade = null;
		if(null != majorVos){
			for(MajorVo item:majorVos){
				for (int i = 1; i < 3; i++) {
					classNo ++;
					grade = new Grade();
					grade.setGradeNo(classNo+"");
					grade.setGradeName("班级"+classNo);
					grade.setMajorNo(item.getMajorNo());
					grade.setHeadmaster("班主任"+classNo);
					gradeMapper.addGrade(grade);
				}
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("运行运行耗时："+(endTime-startTime)+"毫秒");
		return APIResult.newSuccessResult("成功");
	}

	/**
	 * 初始化班级数据
	 * 遍历所有专业，每个专业2个班 20163月份
	 * @return
	 */
	@GetMapping("grade")
	public APIResult initClassData(
	){
		long startTime = System.currentTimeMillis();

		//获取所有专业
		MajorListReq req = new MajorListReq();
		List<MajorVo> majorVos = majorMapper.getMajorList(req);
		//构造数据
		List<Grade> grades = new LinkedList<>();
		Integer classNo = 2016004;
		Grade grade = null;
		if(null != majorVos){
			for(MajorVo item:majorVos){
				for (int i = 1; i < 3; i++) {
					classNo ++;
					grade = new Grade();
					grade.setGradeNo(classNo+"");
					grade.setGradeName("班级"+classNo);
					grade.setMajorNo(item.getMajorNo());
					grade.setHeadmaster("班主任"+classNo);
					grades.add(grade);
				}
			}
		}
		List<Grade> subData = null;
		int batchNum = 200;
		int totalNum = grades.size();
		int pageNum = totalNum % batchNum == 0 ? totalNum / batchNum : totalNum / batchNum + 1;
		ExecutorService executor = Executors.newFixedThreadPool(pageNum);
		try {
			int fromIndex, toIndex;
			final CountDownLatch countDownLatch = new CountDownLatch(pageNum);
			for (int i = 0; i < pageNum; i++) {
				fromIndex = i * batchNum;
				toIndex = Math.min(totalNum, fromIndex + batchNum);
				subData = grades.subList(fromIndex, toIndex);
				ImportTask importTask = new ImportTask(subData,countDownLatch);
				executor.execute(importTask);
			}
			// System.out.println(subData.toString());
			// 主线程必须在启动其它线程后立即调用CountDownLatch.await()方法，
			// 这样主线程的操作就会在这个方法上阻塞，直到其它线程完成各自的任务。
			// 计数器的值等于0时，主线程就能通过await()方法恢复执行自己的任务。
			countDownLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			executor.shutdown();
		}
		long endTime = System.currentTimeMillis();
		System.out.println("运行运行耗时："+(endTime-startTime)+"毫秒");
		return APIResult.newSuccessResult("成功");
	}

	class ImportTask implements Runnable {

		private List<Grade> data;
		private CountDownLatch countDownLatch;
		public ImportTask(List<Grade> data, CountDownLatch countDownLatch){
			this.data = data;
			this.countDownLatch = countDownLatch;
		}
		@Override
		public void run() {
			System.out.println("当前线程名称"+Thread.currentThread().getName());
			if(null != data){
				//业务逻辑
				for(Grade item:data){
					gradeMapper.addGrade(item);
				}
			}
			// 发出线程任务完成的信号
			countDownLatch.countDown();
		}
	}

	/**
	 * 初始化学生数据
	 * 遍历所有班级，每个班级60个学生
	 * @return
	 */
	@GetMapping("student")
	public APIResult initStudentData(){
		long startTime = System.currentTimeMillis();
		//获取所有班级
		List<GradeVo> gradeVos = gradeMapper.getGradeList(null);
		if(null != gradeVos){
			List<Student> students = new LinkedList<>();
			for(GradeVo item:gradeVos){
				for (int i = 1; i < 61; i++) {
					int ran = (int) (Math.random() * 100);
					Student student = new Student();
					student.setStudentNo(item.getGradeNo()+String.format("%03d",i));
					student.setStudentName("学生"+item.getGradeNo()+String.format("%03d",i));
					student.setClassNo(item.getGradeNo());
					student.setEmail(ran+"342342@126.com");
					student.setMobile("132114312"+ran);
					student.setSex(i%2+1);
					students.add(student);
				}
			}
			int batchNum = 1000;
			int totalNum = students.size();
			int pageNum = totalNum % batchNum == 0 ? totalNum / batchNum : totalNum / batchNum + 1;
			ExecutorService executor = Executors.newFixedThreadPool(pageNum);
			try {
				int fromIndex, toIndex;
				final CountDownLatch countDownLatch = new CountDownLatch(pageNum);
				for (int i = 0; i < pageNum; i++) {
					fromIndex = i * batchNum;
					toIndex = Math.min(totalNum, fromIndex + batchNum);
					List<Student> subData = students.subList(fromIndex,toIndex);
					executor.execute(() -> {
						studentMapper.addBatchStudent(subData);
						countDownLatch.countDown();
					});
				}
				countDownLatch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				executor.shutdown();
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("运行运行耗时："+(endTime-startTime)+"毫秒");
		return APIResult.newSuccessResult("成功");
	}

	/**
	 * 初始化成绩数据
	 * 遍历所有班级，每个班级60个学生
	 * @return
	 */
	@GetMapping("scores")
	public APIResult initScoresData(
			@RequestParam(name = "classNo",defaultValue = "") String classNo
	){
		long startTime = System.currentTimeMillis();
		//获取所有课程
		List<CourseVo> courseVos = courseMapper.getCourseList(null);
		// //获取技科班的学生
		StudentListReq studentListReq = new StudentListReq();
		// studentListReq.setClassNo("2016001");
		studentListReq.setClassNo(classNo);

		List<StudentVo> studentVos = studentMapper.getStudentList(studentListReq);

		if(courseVos.size()>0  && studentVos.size()>0 ){
			List<Scores> scoresList = new LinkedList<>();
			Scores scores = null;
			for(CourseVo item:courseVos){
				for(StudentVo stem:studentVos){
					scores = new Scores();
					scores.setCourseNo(item.getCourseNo());
					scores.setStudentNo(stem.getStudentNo());
					scores.setClassNo(stem.getClassNo());
					scores.setScore((int)(Math.random( )*50+50));
					// System.out.println(scores);
					scoresList.add(scores);
				}
			}
			int batchNum = 1000;
			int totalNum = scoresList.size();
			int pageNum = totalNum % batchNum == 0 ? totalNum / batchNum : totalNum / batchNum + 1;
			ExecutorService executor = Executors.newFixedThreadPool(pageNum);
			try {
				int fromIndex, toIndex;
				final CountDownLatch countDownLatch = new CountDownLatch(pageNum);
				for (int i = 0; i < pageNum; i++) {
					fromIndex = i * batchNum;
					toIndex = Math.min(totalNum, fromIndex + batchNum);
					List<Scores> subData = scoresList.subList(fromIndex,toIndex);
					executor.execute(() -> {
						scoresMapper.addBatchScores(subData);
						countDownLatch.countDown();
					});
				}
				countDownLatch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				executor.shutdown();
			}
		}else{
			System.out.println("数据不存在");
		}
		long endTime = System.currentTimeMillis();
		System.out.println("运行运行耗时："+(endTime-startTime)+"毫秒");
		return APIResult.newSuccessResult("执行完毕");
	}

	/**
	 * 批量获取成绩
	 * 1.遍历获取成绩信息
	 * 2.根据成绩关联的学生获取学生信息
	 * 3.根据成绩关联的课程获取课程信息
	 * tips:这里做测试直接调用dao
	 * @return
	 */
	@PostMapping("batchGetScore")
	public APIResult batchGetScore(
			@RequestBody List<Long> scoresListExtReq
	){
		long startTime = System.currentTimeMillis();
		List<Scores> scoresList = new ArrayList<>();
		scoresListExtReq.forEach(x->{
			Scores scores = scoresMapper.getScoresById(x);
			scoresList.add(scores);
		});
		long endTime = System.currentTimeMillis();
		System.out.println("运行运行耗时："+(endTime-startTime)+"毫秒");
		return APIResult.newSuccessResult("执行完毕");
	}
}


/**
 * FileName: CaptchaController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 验证码
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.constant.CacheConstant;
import com.gome.club.scores.common.constant.CommonConstant;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.utils.RedisUtil;
import com.google.code.kaptcha.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping(value = "/sys/captcha/")
public class CaptchaController {

	@Autowired
	private Producer captchaProducer = null;

	@Autowired
	private RedisUtil redisUtil;

	@RequestMapping("/info")
	public void getKaptchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("image/jpeg");
		//生成验证码
		String capText = captchaProducer.createText();
		request.getSession().setAttribute(CommonConstant.LOGIN_CATPCHA, capText);
		//向客户端写出
		BufferedImage bi = captchaProducer.createImage(capText);
		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(bi, "jpg", out);
		try {
			out.flush();
		} finally {
			out.close();
		}
	}

	/**
	 * 测试redis扣库存并发
	 * @return
	 */
	@RequestMapping("/testRedis")
	public APIResult testRedis() throws InterruptedException {
		// int activityId = 10260;
		String total_stock_key = "total_stock_10260";
		String single_stock_key = "single_stock";
		redisUtil.set(total_stock_key, 10000);
		redisUtil.zadd(single_stock_key,"20200927", 9990);

		//多线程测试
		int threadNum = 100;
		ExecutorService executor = Executors.newFixedThreadPool(threadNum);
		try {

			for (int i = 0; i < threadNum; i++) {
				executor.execute(() -> {
					//测试并发扣除总
					for (int j = 0; j < 100; j++) {
						if(redisUtil.decr(total_stock_key,1) < 0){
							System.out.println(Thread.currentThread().getName()+"库存不足!");
							break;
						}
						//判断单场库存是否充足
						if(redisUtil.incrementScore(single_stock_key,"20200927",-1) < 0){
							redisUtil.incr(total_stock_key, 1);
							System.out.println(Thread.currentThread().getName()+"单场库存不足!");
							break;
						}
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			executor.shutdown();
		}
		return APIResult.newSuccessResult("test!!!");
	}

}
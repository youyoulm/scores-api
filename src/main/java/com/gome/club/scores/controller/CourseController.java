/**
 * FileName: CourseController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 课程管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.CourseListReq;
import com.gome.club.scores.dto.CourseOpReq;
import com.gome.club.scores.service.ICourseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/course/")
public class CourseController {

	@Autowired
	private ICourseService iCourseService;

	/**
	 * 获取课程列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getCourseList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "classNo",required = false) String classNo,
			@RequestParam(name = "semester",required = false) String semester
	){
		CourseListReq courseListReq = new CourseListReq();
		courseListReq.setPageNum(pageNum);
		courseListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(classNo)){
			courseListReq.setClassNo(classNo);
		}
		if(StringUtils.isNotBlank(semester)){
			courseListReq.setSemester(semester);
		}
		return iCourseService.getCourseList(courseListReq);
	}

	/**
	 * 查看课程信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getCourseInfo(
			@RequestParam(name = "id",required = false) Integer id
	){
		return iCourseService.getCourseById(id);
	}


	/**
	 * 添加课程信息
	 * @param courseOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addCourse(
			@RequestBody CourseOpReq courseOpReq
	){
		return iCourseService.addCourse(courseOpReq);
	}

	/**
	 * 修改课程信息
	 */
	@PostMapping("update")
	public APIResult updateCourse(
			@RequestBody CourseOpReq courseOpReq
	){
		return iCourseService.updateCourse(courseOpReq);
	}

	/**
	 * 删除课程信息
	 */
	@PostMapping("delete")
	public APIResult deleteCourse(
			@RequestBody CourseOpReq courseOpReq
	){
		if(null == courseOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iCourseService.deleteCourse(courseOpReq.getId());
	}
}
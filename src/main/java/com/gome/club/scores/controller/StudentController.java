/**
 * FileName: StudentController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 学生管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.StudentListReq;
import com.gome.club.scores.dto.StudentOpReq;
import com.gome.club.scores.service.IStudentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/student/")
public class StudentController {

	@Autowired
	private IStudentService iStudentService;

	/**
	 * 获取学生列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getStudentList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "startTime",required = false) String startTime,
			@RequestParam(name = "endTime",required = false) String endTime,
			@RequestParam(name = "studentNo",required = false) String studentNo,
			@RequestParam(name = "studentName",required = false) String studentName,
			@RequestParam(name = "classNo",required = false) String classNo
	){
		StudentListReq studentListReq = new StudentListReq();
		studentListReq.setPageNum(pageNum);
		studentListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(studentNo)){
			studentListReq.setStudentNo(studentNo);
		}
		if(StringUtils.isNotBlank(studentName)){
			studentListReq.setStudentName(studentName);
		}
		if(StringUtils.isNotBlank(classNo)){
			studentListReq.setClassNo(classNo);
		}
		if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)){
			studentListReq.setStartTime(startTime);
			studentListReq.setEndTime(endTime);
		}
		return iStudentService.getStudentList(studentListReq);
	}

	/**
	 * 查看学生信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getStudentInfo(
			@RequestParam(name = "id",required = false) Long id
	){
		return iStudentService.getStudentById(id);
	}


	/**
	 * 添加学生信息
	 * @param studentOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addStudent(
			@RequestBody StudentOpReq studentOpReq
	){
		return iStudentService.addStudent(studentOpReq);
	}

	/**
	 * 修改学生信息
	 */
	@PostMapping("update")
	public APIResult updateStudent(
			@RequestBody StudentOpReq studentOpReq
	){
		return iStudentService.updateStudent(studentOpReq);
	}

	/**
	 * 删除学生信息
	 */
	@PostMapping("delete")
	public APIResult deleteStudent(
			@RequestBody StudentOpReq studentOpReq
	){
		if(null == studentOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iStudentService.deleteStudent(studentOpReq.getId());
	}
}
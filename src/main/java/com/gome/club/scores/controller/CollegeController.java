/**
 * FileName: CollegeController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 学院管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.CollegeListReq;
import com.gome.club.scores.dto.CollegeOpReq;
import com.gome.club.scores.service.ICollegeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/college/")
public class CollegeController {

	@Autowired
	private ICollegeService iCollegeService;

	/**
	 * 获取学院列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getCollegeList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "collegeNo",required = false) String collegeNo,
			@RequestParam(name = "collegeName",required = false) String collegeName
	){
		CollegeListReq collegeListReq = new CollegeListReq();
		collegeListReq.setPageNum(pageNum);
		collegeListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(collegeNo)){
			collegeListReq.setCollegeNo(collegeNo);
		}
		if(StringUtils.isNotBlank(collegeName)){
			collegeListReq.setCollegeName(collegeName);
		}
		return iCollegeService.getCollegeList(collegeListReq);
	}

	/**
	 * 查看学院信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getCollegeInfo(
			@RequestParam(name = "id",required = false) Integer id
	){
		return iCollegeService.getCollegeById(id);
	}


	/**
	 * 添加学院信息
	 * @param collegeOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addCollege(
			@RequestBody CollegeOpReq collegeOpReq
	){
		return iCollegeService.addCollege(collegeOpReq);
	}

	/**
	 * 修改学院信息
	 */
	@PostMapping("update")
	public APIResult updateCollege(
			@RequestBody CollegeOpReq collegeOpReq
	){
		return iCollegeService.updateCollege(collegeOpReq);
	}

	/**
	 * 删除学院信息
	 */
	@PostMapping("delete")
	public APIResult deleteCollege(
			@RequestBody CollegeOpReq collegeOpReq
	){
		if(null == collegeOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iCollegeService.deleteCollege(collegeOpReq.getId());
	}
}
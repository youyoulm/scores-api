/**
 * FileName: ScoresController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 成绩管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.ScoresListReq;
import com.gome.club.scores.dto.ScoresOpReq;
import com.gome.club.scores.entity.Author;
import com.gome.club.scores.service.ICommonService;
import com.gome.club.scores.service.IScoresService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/sys/scores/")
public class ScoresController {

	@Autowired
	private IScoresService iScoresService;

	@Autowired
	private ICommonService iCommonService;

	/**
	 * 获取成绩列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getScoresList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "studentNo",required = false) String studentNo,
			@RequestParam(name = "classNo",required = false) String classNo,
			@RequestParam(name = "courseNo",required = false) String courseNo,
			HttpServletRequest request
	){
		ScoresListReq scoresListReq = new ScoresListReq();
		scoresListReq.setPageNum(pageNum);
		scoresListReq.setPageSize(pageSize);

		//设置数据权限，学生只能看到自己的成绩
		Author author = iCommonService.getLoginInfo(request);
		if(author.getRole() == 3){
			scoresListReq.setStudentNo(author.getUserName());
		}else{

			if(StringUtils.isNotBlank(studentNo)){
				scoresListReq.setStudentNo(studentNo);
			}

			if(StringUtils.isNotBlank(classNo)){
				scoresListReq.setClassNo(classNo);
			}

			if(StringUtils.isNotBlank(courseNo)){
				scoresListReq.setCourseNo(courseNo);
			}
		}
		return iScoresService.getScoresList(scoresListReq);
	}

	/**
	 * 查看成绩信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getScoresInfo(
			@RequestParam(name = "id",required = false) Long id
	){
		return iScoresService.getScoresById(id);
	}


	/**
	 * 添加成绩信息
	 * @param scoresOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addScores(
			@RequestBody ScoresOpReq scoresOpReq
	){
		return iScoresService.addScores(scoresOpReq);
	}

	/**
	 * 修改成绩信息
	 */
	@PostMapping("update")
	public APIResult updateScores(
			@RequestBody ScoresOpReq scoresOpReq
	){
		return iScoresService.updateScores(scoresOpReq);
	}

	/**
	 * 删除成绩信息
	 */
	@PostMapping("delete")
	public APIResult deleteScores(
			@RequestBody ScoresOpReq scoresOpReq
	){
		if(null == scoresOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iScoresService.deleteScores(scoresOpReq.getId());
	}
}
/**
 * FileName: TeacherController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 教师管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.TeacherListReq;
import com.gome.club.scores.dto.TeacherOpReq;
import com.gome.club.scores.service.ITeacherService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/teacher/")
public class TeacherController {

	@Autowired
	private ITeacherService iTeacherService;

	/**
	 * 获取教师列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getTeacherList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "startTime",required = false) String startTime,
			@RequestParam(name = "endTime",required = false) String endTime,
			@RequestParam(name = "teacherNo",required = false) String teacherNo,
			@RequestParam(name = "teacherName",required = false) String teacherName
	){
		TeacherListReq teacherListReq = new TeacherListReq();
		teacherListReq.setPageNum(pageNum);
		teacherListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(teacherNo)){
			teacherListReq.setTeacherNo(teacherNo);
		}
		if(StringUtils.isNotBlank(teacherName)){
			teacherListReq.setTeacherName(teacherName);
		}

		if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)){
			teacherListReq.setStartTime(startTime);
			teacherListReq.setEndTime(endTime);
		}
		return iTeacherService.getTeacherList(teacherListReq);
	}

	/**
	 * 查看教师信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getTeacherInfo(
			@RequestParam(name = "id",required = false) Long id
	){
		return iTeacherService.getTeacherById(id);
	}


	/**
	 * 添加教师信息
	 * @param teacherOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addTeacher(
			@RequestBody TeacherOpReq teacherOpReq
	){
		return iTeacherService.addTeacher(teacherOpReq);
	}

	/**
	 * 修改教师信息
	 */
	@PostMapping("update")
	public APIResult updateTeacher(
			@RequestBody TeacherOpReq teacherOpReq
	){
		return iTeacherService.updateTeacher(teacherOpReq);
	}

	/**
	 * 删除教师信息
	 */
	@PostMapping("delete")
	public APIResult deleteTeacher(
			@RequestBody TeacherOpReq teacherOpReq
	){
		if(null == teacherOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iTeacherService.deleteTeacher(teacherOpReq.getId());
	}
}
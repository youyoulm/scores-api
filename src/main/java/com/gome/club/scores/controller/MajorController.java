/**
 * FileName: MajorController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 专业管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.MajorListReq;
import com.gome.club.scores.dto.MajorOpReq;
import com.gome.club.scores.service.IMajorService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/major/")
public class MajorController {

	@Autowired
	private IMajorService iMajorService;

	/**
	 * 获取专业列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getMajorList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "majorNo",required = false) String majorNo,
			@RequestParam(name = "majorName",required = false) String majorName,
			@RequestParam(name = "collegeNo",required = false) String collegeNo,
			@RequestParam(name = "collegeName",required = false) String collegeName
	){
		MajorListReq majorListReq = new MajorListReq();
		majorListReq.setPageNum(pageNum);
		majorListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(majorNo)){
			majorListReq.setMajorNo(majorNo.trim());
		}
		if(StringUtils.isNotBlank(majorName)){
			majorListReq.setMajorName(majorName.trim());
		}
		if(StringUtils.isNotBlank(collegeNo)){
			majorListReq.setCollegeNo(collegeNo.trim());
		}
		if(StringUtils.isNotBlank(collegeName)){
			majorListReq.setCollegeName(collegeName.trim());
		}
		return iMajorService.getMajorList(majorListReq);
	}

	/**
	 * 查看专业信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getMajorInfo(
			@RequestParam(name = "id",required = false) Integer id
	){
		return iMajorService.getMajorById(id);
	}


	/**
	 * 添加专业信息
	 * @param majorOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addMajor(
			@RequestBody MajorOpReq majorOpReq
	){
		return iMajorService.addMajor(majorOpReq);
	}

	/**
	 * 修改专业信息
	 */
	@PostMapping("update")
	public APIResult updateMajor(
			@RequestBody MajorOpReq majorOpReq
	){
		return iMajorService.updateMajor(majorOpReq);
	}

	/**
	 * 删除专业信息
	 */
	@PostMapping("delete")
	public APIResult deleteMajor(
			@RequestBody MajorOpReq majorOpReq
	){
		if(null == majorOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		return iMajorService.deleteMajor(majorOpReq.getId());
	}

}
/**
 * FileName: CaptchaController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 验证码
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.constant.CommonConstant;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.AuthorCheckReq;
import com.gome.club.scores.dto.AuthorListReq;
import com.gome.club.scores.dto.AuthorOpReq;
import com.gome.club.scores.dto.AuthorPwdReq;
import com.gome.club.scores.entity.Author;
import com.gome.club.scores.service.IAuthorService;
import com.gome.club.scores.utils.CookieUtil;
import com.gome.club.scores.utils.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(value = "/sys/author/")
public class AuthorController {

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private IAuthorService iAuthorService;

	/**
	 * 获取账户列表
	 * @return
	 */
	@GetMapping("list")
	public PageResult getAuthorList(
			@RequestParam(name = "pageNum",required = false,defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize",required = false,defaultValue = "10") Integer pageSize,
			@RequestParam(name = "userName",required = false) String userName
	){
		AuthorListReq authorListReq = new AuthorListReq();
		authorListReq.setPageNum(pageNum);
		authorListReq.setPageSize(pageSize);
		if(StringUtils.isNotBlank(userName)){
			authorListReq.setUserName(userName);
		}
		return iAuthorService.getAuthorList(authorListReq);
	}

	/**
	 * 查看账户信息
	 * @param id
	 * @return
	 */
	@GetMapping("info")
	public APIResult getAuthorInfo(
			@RequestParam(name = "id",required = false) Long id
	){
		AuthorCheckReq req = new AuthorCheckReq();
		req.setId(id);
		return APIResult.newSuccessResult(iAuthorService.getAuthorByName(req));
	}


	/**
	 * 添加账户信息
	 * @param authorOpReq
	 * @return
	 */
	@PostMapping("add")
	public APIResult addAuthor(
			@RequestBody AuthorOpReq authorOpReq
	){
		return iAuthorService.addAuthor(authorOpReq);
	}

	/**
	 * 修改账户信息
	 */
	@PostMapping("update")
	public APIResult updateAuthor(
			@RequestBody AuthorOpReq authorOpReq
	){
		return iAuthorService.updateAuthor(authorOpReq);
	}

	/**
	 * 修改密码
	 */
	@PostMapping("updatePwd")
	public APIResult updatePwd(
			@RequestBody AuthorPwdReq authorPwdReq
	){
		AuthorCheckReq req = new AuthorCheckReq();
		req.setId(authorPwdReq.getId());
		req.setPassword(authorPwdReq.getOldPassword());
		//校验旧密码输入是否正确
		Author author = iAuthorService.getAuthorByName(req);
		if(null == author){
			return APIResult.newFailResult(ErrorCode.UPDATE_PASSWORD_OLD_ERR);
		}
		//更改密码
		AuthorOpReq authorOpReq = new AuthorOpReq();
		authorOpReq.setId(authorPwdReq.getId());
		authorOpReq.setPassword(authorPwdReq.getNewPassword());

		return iAuthorService.updateAuthor(authorOpReq);
	}

	/**
	 * 删除账户信息
	 */
	@PostMapping("delete")
	public APIResult deleteAuthor(
			@RequestBody AuthorOpReq authorOpReq
	){
		if(null == authorOpReq.getId()){
			return APIResult.newFailResult(ErrorCode.COMMON_INVALID_PARAM);
		}
		//删除
		return iAuthorService.deleteAuthor(authorOpReq.getUserName());
	}

	/**
	 * 登录
	 * @param req
	 * @return
	 */
	@PostMapping("login")
	public APIResult login(
			@RequestBody AuthorCheckReq req,
			HttpServletRequest request,
			HttpServletResponse response
	){
		//转化成小写字母
		String code = req.getCaptcha().toLowerCase();

		String captchaCode = (String) request.getSession().getAttribute(CommonConstant.LOGIN_CATPCHA);
		//校验验证码是否正确
		if(!code.equals(captchaCode)){
			return APIResult.newFailResult(ErrorCode.LOGIN_CAPTCHA_FAILED);
		}

		//校验账号是否存在
		Author author = iAuthorService.getAuthorByName(req);
		if (null == author){
			return APIResult.newFailResult(ErrorCode.LOGIN_AUTHOR_NO_EXIST);
		}

		//登录标识写入session
		request.getSession().setAttribute(CommonConstant.IS_LOGIN, author);
		request.getSession().setMaxInactiveInterval(3600);  //设置过期时间，以秒为单位，即在没有活动3600秒后，session过期

		//设置cookie信息
		CookieUtil.setCookie(response,CommonConstant.LOGIN_USER_NAME,author.getUserName(),CommonConstant.LOGIN_USER_NAME_EXPIRE);
		CookieUtil.setCookie(response,CommonConstant.LOGIN_USER_ID,author.getId().toString(),CommonConstant.LOGIN_USER_ID_EXPIRE);

		return APIResult.newSuccessResult(true);
	}

	/**
	 * 退出登录
	 * @param request
	 * @return
	 */
	@PostMapping("logout")
	public APIResult logout(
			HttpServletRequest request,
			HttpServletResponse response
	){
		//清除session
		request.getSession().removeAttribute(CommonConstant.IS_LOGIN);
		//清除cookie
		CookieUtil.deleteCookie(response,CommonConstant.LOGIN_USER_NAME);
		CookieUtil.deleteCookie(response,CommonConstant.LOGIN_USER_ID);

		return APIResult.newSuccessResult(true);
	}

}
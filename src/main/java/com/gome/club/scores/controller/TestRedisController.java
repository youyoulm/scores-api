/**
 * FileName: StudentController
 * Author:   liluming
 * Date:     2020/7/24 3:30 下午
 * Description: 学生管理
 */
package com.gome.club.scores.controller;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/redis/")
public class TestRedisController {

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	/**
	 * 测试redis
	 * @return
	 */
	@GetMapping("test")
	public APIResult getStudentInfo(){
		redisUtil.set("score_class_num","100");
		Object scoreClassum = redisUtil.get("score_class_num");
		return APIResult.newSuccessResult(scoreClassum);
	}

	/**
	 * 测试redis
	 * @return
	 */
	@GetMapping("test_cluster")
	public APIResult testCluster(){
		stringRedisTemplate.opsForValue().set("test_name","liluming666");
		System.out.println(stringRedisTemplate.opsForValue().get("test_name"));
		return APIResult.newSuccessResult("test success");
	}
}
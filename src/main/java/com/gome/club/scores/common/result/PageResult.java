/**
 * FileName: PageResult
 * Author:   liluming
 * Date:     2020/7/29 4:45 下午
 * Description:
 */
package com.gome.club.scores.common.result;

import com.gome.club.scores.common.enums.ErrorCode;
import java.util.Collection;

public class PageResult<T extends Collection> extends ResultSupport {

	protected T data;

	private Meta pageInfo;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Meta getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Meta pageInfo) {
		this.pageInfo = pageInfo;
	}

	public PageResult() {
	}

	public PageResult(int pageNo, int pageSize, Long totalCount, T data) {
		Meta meta = new Meta(pageNo, pageSize, totalCount);
		this.setPageInfo(meta);
		this.setData(data);
	}

	/**
	 * 接口调用失败,有错误字符串码和描述
	 * @param errorCode
	 * @param <U>
	 * @return
	 */
	public static <U extends Collection> PageResult<U> newFailResult(ErrorCode errorCode) {
		PageResult<U> pageResult = new PageResult<U>();
		pageResult.setCode(errorCode.getCode());
		pageResult.setMessage(errorCode.getErrorInfo());
		return pageResult;
	}

	/**
	 * 处理成功信息
	 * @param pageNum
	 * @param pageSize
	 * @param totalCount
	 * @param data
	 * @param <U>
	 * @return
	 */
	public static <U extends Collection> PageResult<U> newSuccessResult(int pageNum, int pageSize, long totalCount, U data) {
		return new PageResult<U>(pageNum, pageSize, totalCount, data);
	}
}
/**
 * FileName: ResultSupport
 * Author:   liluming
 * Date:     2020/6/10 3:44 下午
 * Description: 基类
 */
package com.gome.club.scores.common.result;


/**
 * @desc 基类 *
 * @author liluming
 * @create_date 2020/6/10 3:44 下午
 * @version 1.0
 */

import java.io.Serializable;

public class ResultSupport implements Serializable {
	private static final long serialVersionUID = -2235152751651905167L;

	public boolean isSuccess() {
		return code == ResultCode.SuccessCode;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	private String message = "";
	private int code;
}
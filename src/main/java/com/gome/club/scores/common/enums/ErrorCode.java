/**
 * FileName: ErrorCode
 * Author:   liluming
 * Date:     2020/7/29 11:35 上午
 * Description: 错误码
 */
package com.gome.club.scores.common.enums;


/**
 * @desc 错误码 *
 * @author liluming
 * @create_date 2020/7/29 11:35 上午
 * @version 1.0
 */
public enum ErrorCode {
	//通用提示
	COMMON_INVALID_PARAM(1000, "请求参数错误"),
	COMMON_NO_LOGIN(1002, "用户未登录"),
	//登录提示
	LOGIN_CAPTCHA_FAILED(2001,"验证码错误"),
	LOGIN_AUTHOR_NO_EXIST(2002,"用户名密码错误"),

	//更新密码
	UPDATE_PASSWORD_OLD_ERR(2003,"旧密码输入错误"),
	//教师管理
	CREATE_INFO_FAILED(1001,"创建失败"),
	UPDATE_INFO_FAILED(1002,"修改失败"),
	DELETE_INFO_FAILED(1003,"修改失败"),
	SELECT_INFO_FAILED(1004,"信息获取失败"),
	SELECT_LIST_FAILED(1005,"列表获取失败"),

	;

	/**
	 * 系统内部错误码
	 */
	private Integer code;

	/**
	 * 错误信息
	 */
	private String errorInfo;

	ErrorCode(Integer code, String errorInfo) {
		this.code = code;
		this.errorInfo = errorInfo;
	}

	public Integer getCode() {
		return code;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void appendToMsg(String msg) {
		errorInfo += errorInfo + msg;
	}
}
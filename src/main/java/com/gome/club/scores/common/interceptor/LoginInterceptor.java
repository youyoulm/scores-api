/**
 * FileName: LoginInterceptor
 * Author:   liluming
 * Date:     2020/8/14 2:08 下午
 * Description: 登录拦截器
 */
package com.gome.club.scores.common.interceptor;


import com.alibaba.fastjson.JSON;
import com.gome.club.scores.common.constant.CommonConstant;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.entity.Author;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * @desc 登录拦截器 *
 * @author liluming
 * @create_date 2020/8/14 2:08 下午
 * @version 1.0
 */
public class LoginInterceptor implements HandlerInterceptor {
	/**
	 * 在请求处理之前进行调用（Controller方法调用之前）
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

		// 统一拦截（查询当前session是否存在passport）(这里passport会在每次登陆成功后，写入session)
		Author author=(Author)request.getSession().getAttribute(CommonConstant.IS_LOGIN);
		if(author != null){
			return true;
		}
		System.out.println("------------登录失效------------");
		setReturn(request,response);
		return false;
		// return true;
	}


	/**
	 * 返回错误信息
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private static void setReturn(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpServletResponse httpResponse = response;
		String origin = request.getHeader("Origin");
		httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
		httpResponse.setHeader("Access-Control-Allow-Origin", origin);
		//UTF-8编码
		httpResponse.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=utf-8");
		// Result build = Result.build(status, msg);

		// PageResult<Collection> collectionPageResult = new PageResult<>();
		// collectionPageResult.setPageInfo();
		String json = JSON.toJSONString(PageResult.newFailResult(ErrorCode.COMMON_NO_LOGIN));
		httpResponse.getWriter().print(json);
	}

	/**
	 * 设置返回Header
	 * @param request
	 * @param response
	 */
	// private void setCorsMappings(HttpServletRequest request, HttpServletResponse response){
	// 	String origin = request.getHeader("Origin");
	// 	response.setHeader("Access-Control-Allow-Origin", origin);
	// 	response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	// 	response.setHeader("Access-Control-Max-Age", "3600");
	// 	response.setHeader("Access-Control-Allow-Headers", "x-requested-with,Authorization");
	// 	response.setHeader("Access-Control-Allow-Credentials", "true");
	// }

	/**
	 * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        System.out.println("执行了LoginInterceptor的postHandle方法");
	}

	/**
	 * 在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
       System.out.println("执行了LoginInterceptor的afterCompletion方法");
	}
}
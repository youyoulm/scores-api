/**
 * FileName: CacheConstant
 * Author:   liluming
 * Date:     2020/8/13 11:54 上午
 * Description: 缓存常量
 */
package com.gome.club.scores.common.constant;
import java.util.HashMap;

/**
 * @desc 缓存常量 *
 * @author liluming
 * @create_date 2020/8/13 11:54 上午
 * @version 1.0
 */
public final class CacheConstant {
	//缓存key前缀
	public static final String SCORE_PREFIX = "gome:score_";
	//验证码key
	public static final String CAPTCHA = SCORE_PREFIX+"captcha";

	//验证码key
	// public static final HashMap<String, String > CAPTCHA  = new HashMap<String, String>(){{
	// 	put("key",SCORE_PREFIX+"captcha");
	// 	put("expire","60");
	// }};

}
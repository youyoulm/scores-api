/**
 * FileName: ResultCode
 * Author:   liluming
 * Date:     2020/6/10 3:43 下午
 * Description: 状态码
 */
package com.gome.club.scores.common.result;


/**
 * @desc 状态码 *
 * @author liluming
 * @create_date 2020/6/10 3:43 下午
 * @version 1.0
 */
public abstract class ResultCode {
	public static final int SuccessCode = 0;
}
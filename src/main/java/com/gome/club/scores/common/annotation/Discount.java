/**
 * FileName: Discount
 * Author:   liluming
 * Date:     2020/11/13 3:48 下午
 * Description: 折扣注解
 */
package com.gome.club.scores.common.annotation;


import java.lang.annotation.*;

/**
 * @desc 折扣注解 *
 * @author liluming
 * @create_date 2020/11/13 3:48 下午
 * @version 1.0
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Discount {
	/**
	 * 优惠券类型
	 *
	 * @return
	 */
	String couponType();
}
/**
 * FileName: PageInfo
 * Author:   liluming
 * Date:     2020/7/29 5:15 下午
 * Description: 分页信息
 */
package com.gome.club.scores.common.result;


import lombok.Data;

import java.io.Serializable;

/**
 * @desc 分页信息 *
 * @author liluming
 * @create_date 2020/7/29 5:15 下午
 * @version 1.0
 */
@Data
public class Meta implements Serializable {

	private static final long serialVersionUID = -8002024192068404252L;

	private int pageNum;

	private int pageSize;

	private long total;

	public Meta(int pageNum, int pageSize, long total) {
		this.pageNum = pageNum;
		this.pageSize = pageSize;
		this.total = total;
	}
}
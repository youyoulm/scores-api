/**
 * FileName: APIResult
 * Author:   liluming
 * Date:     2020/6/10 3:45 下午
 * Description: 普通数据
 */
package com.gome.club.scores.common.result;


import com.gome.club.scores.common.enums.ErrorCode;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc 普通数据 *
 * @author liluming
 * 继承自ResultSupport，多了data字段，可以存储数组和普通对象
 * @create_date 2020/6/10 3:45 下午
 * @version 1.0
 */
public class APIResult<T> extends ResultSupport {
	protected T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}


	/**
	 * 处理失败信息
	 * @param error
	 * @param <U>
	 * @return
	 */
	public static <U> APIResult<U> newFailResult(ErrorCode error) {
		APIResult<U> apiResult = new APIResult<U>();
		apiResult.setCode(error.getCode());
		apiResult.setMessage(error.getErrorInfo());
		return apiResult;
	}

	/**
	 * 处理成功信息
	 * @param data
	 * @param <U>
	 * @return
	 */
	public static <U> APIResult<U> newSuccessResult(U data){
		APIResult<U> apiResult = new APIResult<U>();
		apiResult.setData(data);
		return apiResult;
	}
}
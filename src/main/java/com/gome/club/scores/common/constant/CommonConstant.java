/**
 * FileName: CacheConstant
 * Author:   liluming
 * Date:     2020/8/13 11:54 上午
 * Description: 缓存常量
 */
package com.gome.club.scores.common.constant;

/**
 * @desc 公共常量 *
 * @author liluming
 * @create_date 2020/8/13 11:54 上午
 * @version 1.0
 */
public final class CommonConstant {
	//验证码
	public static final String LOGIN_CATPCHA = "captchaCode";
	//登录session标识
	public static final String IS_LOGIN = "passport";
	//账号cookie
	public static final String LOGIN_USER_NAME = "userName";
	public static final Integer LOGIN_USER_NAME_EXPIRE = 24 * 60 * 60; //1天有效期
	//账号id cookie
	public static final String LOGIN_USER_ID = "userId";
	public static final Integer LOGIN_USER_ID_EXPIRE = 24 * 60 * 60;
}
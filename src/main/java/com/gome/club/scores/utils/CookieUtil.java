/**
 * FileName: CookieUtil
 * Author:   liluming
 * Date:     2020/8/18 4:02 下午
 * Description: cookie工具类
 */
package com.gome.club.scores.utils;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @desc cookie工具类 *
 * @author liluming
 * @create_date 2020/8/18 4:02 下午
 * @version 1.0
 */
public class CookieUtil {

	/**
	 * 获取cookie
	 * @param request
	 * @param cookieName
	 * @return
	 */
	public static String getCookie(HttpServletRequest request, String cookieName){

		Cookie[] cookies =  request.getCookies();
		if(cookies != null){
			for(Cookie cookie : cookies){
				if(cookie.getName().equals(cookieName)){
					return cookie.getValue();
				}
			}
		}

		return null;
	}

	/**
	 * 设置cookie
	 * @param response
	 * @param cookieName
	 * @param value
	 * @param cookieMaxAge
	 */
	public static void setCookie(HttpServletResponse response, String cookieName, String value, int cookieMaxAge){
		Cookie cookie = new Cookie(cookieName,value);
		cookie.setPath("/");
		cookie.setMaxAge(cookieMaxAge);
		response.addCookie(cookie);
	}

	/**
	 * 删除cookie
	 * @param response
	 * @param cookieName
	 */
	public static void deleteCookie(HttpServletResponse response, String cookieName){
		setCookie(response,cookieName,null,0);
	}
}
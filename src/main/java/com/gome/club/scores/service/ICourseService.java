/**
 * FileName: ICourseService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 课程管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.CourseListReq;
import com.gome.club.scores.dto.CourseOpReq;
import com.gome.club.scores.entity.Course;
import com.gome.club.scores.vo.CourseVo;

import java.util.List;

/**
 * @desc 课程管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface ICourseService {

	/**
	 * 课程列表
	 * @param req
	 * @return
	 */
	PageResult<List<CourseVo>> getCourseList(CourseListReq req);

	/**
	 * 课程信息ById
	 * @param id
	 * @return
	 */
	APIResult<Course> getCourseById(Integer id);

	/**
	 * 添加课程信息
	 */
	APIResult<Boolean> addCourse(CourseOpReq req);


	/**
	 * 修改课程信息
	 */
	APIResult<Boolean> updateCourse(CourseOpReq req);

	/**
	 * 删除课程信息
	 */
	APIResult<Boolean> deleteCourse(Integer id);
}
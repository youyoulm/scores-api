/**
 * FileName: AuthorImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 账号管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.AuthorMapper;
import com.gome.club.scores.dto.AuthorCheckReq;
import com.gome.club.scores.dto.AuthorListReq;
import com.gome.club.scores.dto.AuthorOpReq;
import com.gome.club.scores.entity.Author;
import com.gome.club.scores.service.IAuthorService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @desc 账号管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class AuthorServiceImpl implements IAuthorService {

	@Autowired
	private AuthorMapper authorMapper;

	public PageResult<List<Author>> getAuthorList(AuthorListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<Author> authorVos = authorMapper.getAuthorList(req);
		PageInfo<Author> pageInfo = new PageInfo<>(authorVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),authorVos);
	}

	public Author getAuthorByName(AuthorCheckReq req) {
		Author author = authorMapper.getAuthorByName(req);
		return author;
	}

	public APIResult<Boolean> addAuthor(AuthorOpReq authorOpReq) {
		Author author = new Author();
		BeanUtils.copyProperties(authorOpReq,author);

		int result = authorMapper.addAuthor(author);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> updateAuthor(AuthorOpReq authorOpReq) {
		Author author = new Author();
		BeanUtils.copyProperties(authorOpReq,author);

		int result = authorMapper.updateAuthor(author);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> deleteAuthor(String userName) {
		int result = authorMapper.deleteAuthor(userName);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}
}
/**
 * FileName: MJCoupon
 * Author:   liluming
 * Date:     2020/11/3 3:30 下午
 * Description: 满减券
 */
package com.gome.club.scores.service.coupon.impl;


import com.gome.club.scores.common.annotation.Discount;
import com.gome.club.scores.service.coupon.ICoupon;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @desc 直减券 *
 * @author liluming
 * @create_date 2020/11/3 3:30 下午
 * @version 1.0
 */
@Discount(couponType = "MJ")
@Component(value = "MJ2")
public class MJCoupon implements ICoupon<Map<String, String>> {
	/**
	 * 满减计算
	 * 1.判断满足x元后-n元，否则不减
	 * 2.最低支付金额1元
	 */
	@Override
	public BigDecimal discountAmount(Map<String, String> couponInfo, BigDecimal skuPrice) {
		String x = couponInfo.get("x");
		String o = couponInfo.get("o");

		// 小于商品金额，直接返回商品金额
		if (skuPrice.compareTo(new BigDecimal(x)) < 0) {
			return skuPrice;
		}

		// 减去优惠金额
		BigDecimal discountAmount = skuPrice.subtract(new BigDecimal(o));
		if (discountAmount.compareTo(BigDecimal.ZERO) < 1) {
			return BigDecimal.ONE;
		}

		return discountAmount;
	}
}
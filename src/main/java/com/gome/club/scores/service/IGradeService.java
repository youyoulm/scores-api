/**
 * FileName: IGradeService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 班级管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.GradeListReq;
import com.gome.club.scores.dto.GradeOpReq;
import com.gome.club.scores.entity.Grade;
import com.gome.club.scores.vo.GradeVo;

import java.util.List;

/**
 * @desc 班级管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface IGradeService {

	/**
	 * 班级列表
	 * @param req
	 * @return
	 */
	PageResult<List<GradeVo>> getGradeList(GradeListReq req);

	/**
	 * 班级信息ById
	 * @param id
	 * @return
	 */
	APIResult<Grade> getGradeById(Integer id);

	/**
	 * 添加班级信息
	 */
	APIResult<Boolean> addGrade(GradeOpReq req);


	/**
	 * 修改班级信息
	 */
	APIResult<Boolean> updateGrade(GradeOpReq req);

	/**
	 * 删除班级信息
	 */
	APIResult<Boolean> deleteGrade(Integer id);
}
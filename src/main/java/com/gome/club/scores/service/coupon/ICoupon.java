/**
 * FileName: ICoupon
 * Author:   liluming
 * Date:     2020/11/13 3:39 下午
 * Description: 优惠券计算
 */
package com.gome.club.scores.service.coupon;


import java.math.BigDecimal;

/**
 * @desc 各类券接口类 *
 * @author liluming
 * @create_date 2020/11/3 3:30 下午
 * @version 1.0
 */
public interface ICoupon<T> {
	/**
	 * 优惠金额计算
	 * @param couponInfo 券折扣信息，满减，直减，折扣，N元购
	 * @param skuPrice
	 * @return
	 */
	BigDecimal discountAmount(T couponInfo, BigDecimal skuPrice);
}
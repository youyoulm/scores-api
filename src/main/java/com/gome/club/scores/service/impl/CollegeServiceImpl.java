/**
 * FileName: CollegeImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 教师管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.CollegeMapper;
import com.gome.club.scores.dto.CollegeListReq;
import com.gome.club.scores.dto.CollegeOpReq;
import com.gome.club.scores.entity.College;
import com.gome.club.scores.service.ICollegeService;
import com.gome.club.scores.vo.CollegeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @desc 教师管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class CollegeServiceImpl implements ICollegeService {

	@Autowired
	private CollegeMapper collegeMapper;

	public PageResult<List<CollegeVo>> getCollegeList(CollegeListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<CollegeVo> collegeVos = collegeMapper.getCollegeList(req);
		PageInfo<CollegeVo> pageInfo = new PageInfo<>(collegeVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),collegeVos);
	}

	public APIResult<College> getCollegeById(Integer id) {
		College college = collegeMapper.getCollegeById(id);
		if(null != college){
			return APIResult.newSuccessResult(college);
		} else {
			return APIResult.newFailResult(ErrorCode.SELECT_INFO_FAILED);
		}

	}

	public APIResult<Boolean> addCollege(CollegeOpReq collegeOpReq) {
		College college = new College();
		BeanUtils.copyProperties(collegeOpReq,college);

		Integer result = collegeMapper.addCollege(college);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> updateCollege(CollegeOpReq collegeOpReq) {
		College college = new College();
		BeanUtils.copyProperties(collegeOpReq,college);

		Integer result = collegeMapper.updateCollege(college);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> deleteCollege(Integer id) {
		Integer result = collegeMapper.deleteCollege(id);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}
}
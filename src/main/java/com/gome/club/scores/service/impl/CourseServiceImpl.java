/**
 * FileName: CourseImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 课程管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.CourseMapper;
import com.gome.club.scores.dto.CourseListReq;
import com.gome.club.scores.dto.CourseOpReq;
import com.gome.club.scores.entity.Course;
import com.gome.club.scores.service.ICourseService;
import com.gome.club.scores.vo.CourseVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @desc 课程管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class CourseServiceImpl implements ICourseService {

	@Autowired
	private CourseMapper courseMapper;

	public PageResult<List<CourseVo>> getCourseList(CourseListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<CourseVo> courseVos = courseMapper.getCourseList(req);
		PageInfo<CourseVo> pageInfo = new PageInfo<>(courseVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),courseVos);
	}

	public APIResult<Course> getCourseById(Integer id) {
		Course course = courseMapper.getCourseById(id);
		if(null != course){
			return APIResult.newSuccessResult(course);
		} else {
			return APIResult.newFailResult(ErrorCode.SELECT_INFO_FAILED);
		}

	}

	public APIResult<Boolean> addCourse(CourseOpReq courseOpReq) {
		Course course = new Course();
		BeanUtils.copyProperties(courseOpReq,course);
		course.setCreateTime(new Date());

		Integer result = courseMapper.addCourse(course);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> updateCourse(CourseOpReq courseOpReq) {
		Course course = new Course();
		BeanUtils.copyProperties(courseOpReq,course);

		Integer result = courseMapper.updateCourse(course);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> deleteCourse(Integer id) {
		Integer result = courseMapper.deleteCourse(id);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}
}
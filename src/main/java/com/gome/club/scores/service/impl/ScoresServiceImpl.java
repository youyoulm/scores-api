/**
 * FileName: ScoresImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 教师管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.ScoresMapper;
import com.gome.club.scores.dto.ScoresListReq;
import com.gome.club.scores.dto.ScoresOpReq;
import com.gome.club.scores.entity.Scores;
import com.gome.club.scores.service.IScoresService;
import com.gome.club.scores.vo.ScoresVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @desc 成绩管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class ScoresServiceImpl implements IScoresService {

	@Autowired
	private ScoresMapper scoresMapper;

	@Override
    public PageResult<List<ScoresVo>> getScoresList(ScoresListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<ScoresVo> scoresVos = scoresMapper.getScoresList(req);
		PageInfo<ScoresVo> pageInfo = new PageInfo<>(scoresVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),scoresVos);
	}

	@Override
	public APIResult<Scores> getScoresById(Long id) {
		Scores scores = scoresMapper.getScoresById(id);
		if(null != scores){
			return APIResult.newSuccessResult(scores);
		} else {
			return APIResult.newFailResult(ErrorCode.SELECT_INFO_FAILED);
		}

	}

	@Override
	public APIResult<Boolean> addScores(ScoresOpReq scoresOpReq) {
		Scores scores = new Scores();
		BeanUtils.copyProperties(scoresOpReq,scores);
		scores.setCreateTime(new Date());

		Integer result = scoresMapper.addScores(scores);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	@Override
	public APIResult<Boolean> updateScores(ScoresOpReq scoresOpReq) {
		Scores scores = new Scores();
		BeanUtils.copyProperties(scoresOpReq,scores);

		Integer result = scoresMapper.updateScores(scores);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	@Override
	public APIResult<Boolean> deleteScores(Long id) {
		Integer result = scoresMapper.deleteScores(id);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}

	@Override
	public List<ScoresVo> batchGetScoresList(List<String> req){
		return null;
	}
}
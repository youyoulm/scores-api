/**
 * FileName: IAuthorService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description:账号管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.entity.Author;

import javax.servlet.http.HttpServletRequest;

/**
 * @desc公共接口 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface ICommonService {

	/**
	 * 获取当前登录账号信息
	 * @return
	 */
	Author getLoginInfo(HttpServletRequest request);
}
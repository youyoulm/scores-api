/**
 * FileName: CouponContext
 * Author:   liluming
 * Date:     2020/11/13 4:02 下午
 * Description: 优惠券策略类
 */
package com.gome.club.scores.service.coupon;

import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @desc 优惠券策略控制类 *
 * 1.扫描包中标有@Discount注解的类(这里也可以替换成扫描继承ICoupon的所有类)
 * 2.将注解中的优惠券类型值作为key，对应的类作为value，初始化handlerMap
 * @author liluming
 * @create_date 2020/11/13 4:02 下午
 * @version 1.0
 */
@Component
public class CouponContext2 {

	/**
	 * 保存处理的map，key为couponType，value为处理器Class
	 */
	@Resource
	private Map<String, ICoupon> HANDLER_MAP;

	/**
	 * 根据类型
	 * @param couponType
	 * @return
	 */
	public ICoupon getCouponHandlerInstance(String couponType) {
		System.out.println(HANDLER_MAP);
		ICoupon iCouponClass = HANDLER_MAP.get(couponType);
		if (iCouponClass == null) {
			throw new RuntimeException("不存在的优惠券类型");
		}
		return iCouponClass;
	}
}
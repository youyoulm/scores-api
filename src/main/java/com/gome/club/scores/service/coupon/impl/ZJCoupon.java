/**
 * FileName: MJCoupon
 * Author:   liluming
 * Date:     2020/11/3 3:30 下午
 * Description: 满减券
 */
package com.gome.club.scores.service.coupon.impl;


import com.gome.club.scores.common.annotation.Discount;
import com.gome.club.scores.service.coupon.ICoupon;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @desc 直减券 *
 * @author liluming
 * @create_date 2020/11/3 3:30 下午
 * @version 1.0
 */
@Discount(couponType = "ZJ")
@Component(value = "ZJ2")
public class ZJCoupon implements ICoupon<Double> {
	/**
	 * 满减计算
	 * 1.使用商品价格减去优惠价格
	 * 2.最低支付金额1元
	 */
	@Override
	public BigDecimal discountAmount(Double couponInfo, BigDecimal skuPrice) {

		// 减去优惠金额
		BigDecimal discountAmount = skuPrice.subtract(new BigDecimal(couponInfo));
		if (discountAmount.compareTo(BigDecimal.ZERO) < 1) {
			return BigDecimal.ONE;
		}
		return discountAmount;
	}
}
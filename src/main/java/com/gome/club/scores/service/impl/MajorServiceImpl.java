/**
 * FileName: MajorImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 教师管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.MajorMapper;
import com.gome.club.scores.dto.MajorListReq;
import com.gome.club.scores.dto.MajorOpReq;
import com.gome.club.scores.entity.Major;
import com.gome.club.scores.service.IMajorService;
import com.gome.club.scores.vo.MajorVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @desc 教师管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class MajorServiceImpl implements IMajorService {

	@Autowired
	private MajorMapper majorMapper;

	public PageResult<List<MajorVo>> getMajorList(MajorListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<MajorVo> majorVos = majorMapper.getMajorList(req);
		PageInfo<MajorVo> pageInfo = new PageInfo<>(majorVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),majorVos);
	}

	public APIResult<Major> getMajorById(Integer id) {
		Major major = majorMapper.getMajorById(id);
		if(null != major){
			return APIResult.newSuccessResult(major);
		} else {
			return APIResult.newFailResult(ErrorCode.SELECT_INFO_FAILED);
		}

	}

	public APIResult<Boolean> addMajor(MajorOpReq majorOpReq) {
		Major major = new Major();
		BeanUtils.copyProperties(majorOpReq,major);
		major.setCreateTime(new Date());

		Integer result = majorMapper.addMajor(major);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> updateMajor(MajorOpReq majorOpReq) {
		Major major = new Major();
		BeanUtils.copyProperties(majorOpReq,major);

		Integer result = majorMapper.updateMajor(major);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> deleteMajor(Integer id) {
		Integer result = majorMapper.deleteMajor(id);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}
}
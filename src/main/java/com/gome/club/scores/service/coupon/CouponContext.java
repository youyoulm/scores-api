/**
 * FileName: CouponContext
 * Author:   liluming
 * Date:     2020/11/13 4:02 下午
 * Description: 优惠券策略类
 */
package com.gome.club.scores.service.coupon;


import com.gome.club.scores.common.annotation.Discount;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc 优惠券策略控制类 *
 * 1.扫描包中标有@Discount注解的类(这里也可以替换成扫描继承ICoupon的所有类)
 * 2.将注解中的优惠券类型值作为key，对应的类作为value，初始化handlerMap
 * @author liluming
 * @create_date 2020/11/13 4:02 下午
 * @version 1.0
 */
@Component
public class CouponContext implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	/**
	 * 保存处理的map，key为couponType，value为处理器Class
	 */
	private static final Map<String, Class> HANDLER_MAP = new HashMap<>(3);

	/**
	 * 根据类型
	 * @param couponType
	 * @return
	 */
	public ICoupon getCouponHandlerInstance(String couponType) {
		Class<?> clazz = HANDLER_MAP.get(couponType);
		if (clazz == null) {
			throw new RuntimeException("不存在的优惠券类型");
		}
		return (ICoupon) applicationContext.getBean(clazz);
	}


	/**
	 * 扫描@Discount注解，初始化CouponContext，将其注册到Spring容器中
	 *
	 * @param applicationContext
	 * @throws BeansException
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		// 遍历所有带有@Discount注解的类
		Map<String, Object> couponHandleBeans = applicationContext.getBeansWithAnnotation(Discount.class);
		if (!CollectionUtils.isEmpty(couponHandleBeans)) {
			for (Object couponHandelBean : couponHandleBeans.values()) {
				String couponType = couponHandelBean.getClass().getAnnotation(Discount.class).couponType();
				HANDLER_MAP.put(couponType, couponHandelBean.getClass());
			}
		}
	}

}
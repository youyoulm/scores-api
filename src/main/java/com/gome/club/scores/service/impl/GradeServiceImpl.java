/**
 * FileName: GradeImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 教师管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.GradeMapper;
import com.gome.club.scores.dto.GradeListReq;
import com.gome.club.scores.dto.GradeOpReq;
import com.gome.club.scores.entity.Grade;
import com.gome.club.scores.service.IGradeService;
import com.gome.club.scores.vo.GradeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @desc 班级管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class GradeServiceImpl implements IGradeService {

	@Autowired
	private GradeMapper gradeMapper;

	public PageResult<List<GradeVo>> getGradeList(GradeListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<GradeVo> gradeVos = gradeMapper.getGradeList(req);
		PageInfo<GradeVo> pageInfo = new PageInfo<>(gradeVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),gradeVos);
	}

	public APIResult<Grade> getGradeById(Integer id) {
		Grade grade = gradeMapper.getGradeById(id);
		if(null != grade){
			return APIResult.newSuccessResult(grade);
		} else {
			return APIResult.newFailResult(ErrorCode.SELECT_INFO_FAILED);
		}

	}

	public APIResult<Boolean> addGrade(GradeOpReq gradeOpReq) {
		Grade grade = new Grade();
		BeanUtils.copyProperties(gradeOpReq,grade);
		grade.setCreateTime(new Date());

		Integer result = gradeMapper.addGrade(grade);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> updateGrade(GradeOpReq gradeOpReq) {
		Grade grade = new Grade();
		BeanUtils.copyProperties(gradeOpReq,grade);

		Integer result = gradeMapper.updateGrade(grade);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> deleteGrade(Integer id) {
		Integer result = gradeMapper.deleteGrade(id);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}
}
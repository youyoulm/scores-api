/**
 * FileName: AuthorImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 账号管理
 */
package com.gome.club.scores.service.impl;

import com.gome.club.scores.common.constant.CommonConstant;
import com.gome.club.scores.entity.Author;
import com.gome.club.scores.service.ICommonService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @desc 账号管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class CommonServiceImpl implements ICommonService {

	/**
	 * 获取当前登录账号信息
	 * @return
	 */
	@Override
	public Author getLoginInfo(HttpServletRequest request){
		return (Author) request.getSession().getAttribute(CommonConstant.IS_LOGIN);
	}
}
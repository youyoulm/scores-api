/**
 * FileName: IStudentService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 学生管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.StudentListReq;
import com.gome.club.scores.dto.StudentOpReq;
import com.gome.club.scores.entity.Student;
import com.gome.club.scores.vo.StudentVo;

import java.util.List;

/**
 * @desc 学生管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface IStudentService {

	/**
	 * 学生列表
	 * @param req
	 * @return
	 */
	PageResult<List<StudentVo>> getStudentList(StudentListReq req);

	/**
	 * 学生信息ById
	 * @param id
	 * @return
	 */
	APIResult<Student> getStudentById(Long id);

	/**
	 * 添加学生信息
	 */
	APIResult<Boolean> addStudent(StudentOpReq req);


	/**
	 * 修改学生信息
	 */
	APIResult<Boolean> updateStudent(StudentOpReq req);

	/**
	 * 删除学生信息
	 */
	APIResult<Boolean> deleteStudent(Long id);
}
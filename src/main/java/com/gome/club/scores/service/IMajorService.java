/**
 * FileName: IMajorService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 专业管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.MajorListReq;
import com.gome.club.scores.dto.MajorOpReq;
import com.gome.club.scores.entity.Major;
import com.gome.club.scores.vo.MajorVo;

import java.util.List;

/**
 * @desc 专业管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface IMajorService {

	/**
	 * 专业列表
	 * @param req
	 * @return
	 */
	PageResult<List<MajorVo>> getMajorList(MajorListReq req);

	/**
	 * 专业信息ById
	 * @param id
	 * @return
	 */
	APIResult<Major> getMajorById(Integer id);

	/**
	 * 添加专业信息
	 */
	APIResult<Boolean> addMajor(MajorOpReq req);


	/**
	 * 修改专业信息
	 */
	APIResult<Boolean> updateMajor(MajorOpReq req);

	/**
	 * 删除专业信息
	 */
	APIResult<Boolean> deleteMajor(Integer id);
}
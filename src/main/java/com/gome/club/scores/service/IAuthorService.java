/**
 * FileName: IAuthorService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description:账号管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.AuthorCheckReq;
import com.gome.club.scores.dto.AuthorListReq;
import com.gome.club.scores.dto.AuthorOpReq;
import com.gome.club.scores.entity.Author;

import java.util.List;

/**
 * @desc账号管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface IAuthorService {

	/**
	 *账号列表
	 * @param req
	 * @return
	 */
	PageResult<List<Author>> getAuthorList(AuthorListReq req);

	/**
	 *账号信息ByName
	 * @param req
	 * @return
	 */
	Author getAuthorByName(AuthorCheckReq req);

	/**
	 * 添加账号信息
	 */
	APIResult<Boolean> addAuthor(AuthorOpReq authorOpReq);


	/**
	 * 修改账号信息
	 */
	APIResult<Boolean> updateAuthor(AuthorOpReq authorOpReq);

	/**
	 * 删除账号信息
	 */
	APIResult<Boolean> deleteAuthor(String userName);
}
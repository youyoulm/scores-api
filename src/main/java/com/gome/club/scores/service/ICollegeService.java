/**
 * FileName: ICollegeService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 学院管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.CollegeListReq;
import com.gome.club.scores.dto.CollegeOpReq;
import com.gome.club.scores.entity.College;
import com.gome.club.scores.vo.CollegeVo;

import java.util.List;

/**
 * @desc 学院管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface ICollegeService {

	/**
	 * 学院列表
	 * @param req
	 * @return
	 */
	PageResult<List<CollegeVo>> getCollegeList(CollegeListReq req);

	/**
	 * 学院信息ById
	 * @param id
	 * @return
	 */
	APIResult<College> getCollegeById(Integer id);

	/**
	 * 添加学院信息
	 */
	APIResult<Boolean> addCollege(CollegeOpReq collegeOpReq);


	/**
	 * 修改学院信息
	 */
	APIResult<Boolean> updateCollege(CollegeOpReq collegeOpReq);

	/**
	 * 删除学院信息
	 */
	APIResult<Boolean> deleteCollege(Integer id);
}
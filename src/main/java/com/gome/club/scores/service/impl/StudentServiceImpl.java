/**
 * FileName: StudentImpl
 * Author:   liluming
 * Date:     2020/7/27 3:10 下午
 * Description: 教师管理
 */
package com.gome.club.scores.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gome.club.scores.common.enums.ErrorCode;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dao.StudentMapper;
import com.gome.club.scores.dto.StudentListReq;
import com.gome.club.scores.dto.StudentOpReq;
import com.gome.club.scores.entity.Student;
import com.gome.club.scores.service.IStudentService;
import com.gome.club.scores.vo.StudentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @desc 教师管理实现层 *
 * @author liluming
 * @create_date 2020/7/27 3:10 下午
 * @version 1.0
 */
@Service
public class StudentServiceImpl implements IStudentService {

	@Autowired
	private StudentMapper studentMapper;

	public PageResult<List<StudentVo>> getStudentList(StudentListReq req) {

		PageHelper.startPage(req.getPageNum(), req.getPageSize());
		List<StudentVo> studentVos = studentMapper.getStudentList(req);
		PageInfo<StudentVo> pageInfo = new PageInfo<>(studentVos);
		return PageResult.newSuccessResult(req.getPageNum(),req.getPageSize(),pageInfo.getTotal(),studentVos);
	}

	public APIResult<Student> getStudentById(Long id) {
		Student student = studentMapper.getStudentById(id);
		if(null != student){
			return APIResult.newSuccessResult(student);
		} else {
			return APIResult.newFailResult(ErrorCode.SELECT_INFO_FAILED);
		}

	}

	public APIResult<Boolean> addStudent(StudentOpReq studentOpReq) {
		Student student = new Student();
		BeanUtils.copyProperties(studentOpReq,student);
		student.setCreateTime(new Date());

		Integer result = studentMapper.addStudent(student);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.CREATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> updateStudent(StudentOpReq studentOpReq) {
		Student student = new Student();
		BeanUtils.copyProperties(studentOpReq,student);

		Integer result = studentMapper.updateStudent(student);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.UPDATE_INFO_FAILED);
		}
	}

	public APIResult<Boolean> deleteStudent(Long id) {
		Integer result = studentMapper.deleteStudent(id);
		if(result > 0) {
			return APIResult.newSuccessResult(true);
		} else {
			return APIResult.newFailResult(ErrorCode.DELETE_INFO_FAILED);
		}
	}
}
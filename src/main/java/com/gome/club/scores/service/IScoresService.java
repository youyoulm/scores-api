/**
 * FileName: IScoresService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 成绩管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.ScoresListReq;
import com.gome.club.scores.dto.ScoresOpReq;
import com.gome.club.scores.entity.Scores;
import com.gome.club.scores.vo.ScoresVo;

import java.util.List;

/**
 * @desc 成绩管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface IScoresService {

	/**
	 * 成绩列表
	 * @param req
	 * @return
	 */
	PageResult<List<ScoresVo>> getScoresList(ScoresListReq req);

	/**
	 * 成绩信息ById
	 * @param id
	 * @return
	 */
	APIResult<Scores> getScoresById(Long id);

	/**
	 * 添加成绩信息
	 */
	APIResult<Boolean> addScores(ScoresOpReq req);


	/**
	 * 修改成绩信息
	 */
	APIResult<Boolean> updateScores(ScoresOpReq req);

	/**
	 * 删除成绩信息
	 */
	APIResult<Boolean> deleteScores(Long id);

	/**
	 * 根据成绩id批量获取成绩列表
	 * @param req
	 * @return
	 */
	List<ScoresVo> batchGetScoresList(List<String> req);
}
/**
 * FileName: ITeacherService
 * Author:   liluming
 * Date:     2020/7/27 3:09 下午
 * Description: 教师管理
 */
package com.gome.club.scores.service;

import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.TeacherListReq;
import com.gome.club.scores.dto.TeacherOpReq;
import com.gome.club.scores.entity.Teacher;
import com.gome.club.scores.vo.TeacherVo;

import java.util.List;

/**
 * @desc 教师管理接口层 *
 * @author liluming
 * @create_date 2020/7/27 3:09 下午
 * @version 1.0
 */
public interface ITeacherService {

	/**
	 * 教师列表
	 * @param req
	 * @return
	 */
	PageResult<List<TeacherVo>> getTeacherList(TeacherListReq req);

	/**
	 * 教师信息ById
	 * @param id
	 * @return
	 */
	APIResult<Teacher> getTeacherById(Long id);

	/**
	 * 添加教师信息
	 */
	APIResult<Boolean> addTeacher(TeacherOpReq teacherOpReq);


	/**
	 * 修改教师信息
	 */
	APIResult<Boolean> updateTeacher(TeacherOpReq teacherOpReq);

	/**
	 * 删除教师信息
	 */
	APIResult<Boolean> deleteTeacher(Long id);
}
/**
 * FileName: MJCoupon
 * Author:   liluming
 * Date:     2020/11/3 3:30 下午
 * Description: 满减券
 */
package com.gome.club.scores.service.coupon.impl;


import com.gome.club.scores.common.annotation.Discount;
import com.gome.club.scores.service.coupon.ICoupon;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @desc 折扣券 *
 * @author liluming
 * @create_date 2020/11/3 3:30 下午
 * @version 1.0
 */
@Discount(couponType = "ZK")
@Component(value = "ZK2")
public class ZKCoupon implements ICoupon<Double> {
	/**
	 * 折扣计算
	 * 1.商品金额*折扣
	 * 2.最低支付金额1元
	 */
	@Override
	public BigDecimal discountAmount(Double couponInfo, BigDecimal skuPrice) {


		// 计算打完折后的金额
		BigDecimal discountAmount = skuPrice.multiply(new BigDecimal(couponInfo)).setScale(2, BigDecimal.ROUND_HALF_UP);
		if (discountAmount.compareTo(BigDecimal.ZERO) < 1) {
			return BigDecimal.ONE;
		}
		return discountAmount;
	}
}
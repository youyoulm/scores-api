package com.gome.club.scores.service.impl;

import com.gome.club.scores.ScoresApplicationTests;
import com.gome.club.scores.common.result.APIResult;
import com.gome.club.scores.common.result.PageResult;
import com.gome.club.scores.dto.StudentListReq;
import com.gome.club.scores.entity.Student;
import com.gome.club.scores.service.IStudentService;
import com.gome.club.scores.service.coupon.CouponContext;
import com.gome.club.scores.service.coupon.CouponContext2;
import com.gome.club.scores.service.coupon.ICoupon;
import com.gome.club.scores.vo.StudentVo;
import org.junit.Assert;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class StudentServiceImplTest extends ScoresApplicationTests {
	@Autowired
	private IStudentService iStudentService;

	@Autowired
	private CouponContext couponContext;

	@Autowired
	private CouponContext2 couponContext2;

	/**
	 * 测试获取学生列表接口
	 */
	@Test
	@Disabled
	void getStudentList() {
		try {
			StudentListReq studentListReq = new StudentListReq();
			studentListReq.setStudentNo("2016001001");
			studentListReq.setPageNum(1);
			studentListReq.setPageSize(10);
			PageResult<List<StudentVo>> studentList = iStudentService.getStudentList(studentListReq);
			Assert.assertTrue("获取失败",studentList.getData() != null);  //断言返回结果
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 测试获取学生详情接口
	 */
	@Test
	@Disabled
	void getStudentById() {
		try {
			Long id = 0L;
			APIResult<Student> student = iStudentService.getStudentById(id);
			Assert.assertTrue("获取失败",student.getData() != null);  //断言返回结果
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	void addStudent() {
		// 测试不同优惠券后对应的金额
		// 测试满减
		Map<String, String> couponInfo = new HashMap<>();
		couponInfo.put("x", "100");
		couponInfo.put("o", "10");
		ICoupon mjObj = couponContext.getCouponHandlerInstance("MJa");
		BigDecimal mjAmount = mjObj.discountAmount(couponInfo, new BigDecimal(120));
		System.out.println("满减后的金额"+mjAmount);

		// 测试直减
		// Double zjcouponInfo = 20D;
		// ICoupon zjObj = couponContext.getCouponHandlerInstance("ZJ");
		// BigDecimal zjAmount = zjObj.discountAmount(zjcouponInfo, new BigDecimal(120));
		// System.out.println("直减后的金额"+zjAmount);

		// 折扣金额
		// Double zkcouponInfo = 0.9D;
		// ICoupon zkObj = couponContext.getCouponHandlerInstance("ZK");
		// BigDecimal zkAmount = zkObj.discountAmount(zkcouponInfo, new BigDecimal(120));
		// System.out.println("折扣金额"+zkAmount);
	}

	@Test
	void addStudent2() {
		// 测试不同优惠券后对应的金额
		// 测试满减
		Map<String, String> couponInfo = new HashMap<>();
		couponInfo.put("x", "100");
		couponInfo.put("o", "10");
		ICoupon mjObj = couponContext2.getCouponHandlerInstance("MJ2");
		BigDecimal mjAmount = mjObj.discountAmount(couponInfo, new BigDecimal(120));
		System.out.println("满减后的金额"+mjAmount);
	}

	// @Test
	// void updateStudent() {
	// 	OrderService orderService = new OrderService();
	// 	// 模拟50个线程
	// 	for (int i = 0; i < 50; i++) {
	// 		new Thread(() -> {
	// 			String str = orderService.getOrderNumber();
	// 			System.out.println(str);
	// 			try {
	// 				Thread.sleep(1000);
	// 			} catch (InterruptedException e) {
	// 				e.printStackTrace();
	// 			}
	// 		}, String.valueOf(i)).start();
	// 	}
	// }
	// @Test
	// @Disabled
	// void deleteStudent() {
	// }
}